set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)
SET(CMAKE_C_COMPILER_WORKS 1)
set(CMAKE_SYSTEM_PROCESSOR arm)

if (MAP_CREATION STREQUAL "ON")
    set(LINK_MAP_CREATION_FLAG "-Wl,-Map=${PROJECT_BINARY_DIR}/${PROJ_NAME}.map")
endif ()

set(TOOLCHAIN_BIN_PATH "${TOOLCHAIN_BIN_DIR}arm-none-eabi")
SET(CMAKE_C_COMPILER ${TOOLCHAIN_BIN_PATH}-gcc)
set(SIZE ${TOOLCHAIN_BIN_PATH}-size)
set(CMAKE_OBJCOPY ${TOOLCHAIN_BIN_PATH}-objcopy)
set(CMAKE_OBJDUMP ${TOOLCHAIN_BIN_PATH}-objdump)

set(MEM_LD "${CMAKE_CURRENT_SOURCE_DIR}/submodules/lib_w7500x/linker/mem.ld")
set(SECTIONS_LD "${CMAKE_CURRENT_SOURCE_DIR}/submodules/lib_w7500x/linker/sections.ld")

set(MC_FLAGS "-mcpu=cortex-m0plus -mthumb -mfloat-abi=soft")

set(COMMON_FLAGS "${MC_FLAGS} -Wall -Wextra  -fdata-sections -ffunction-sections -Wl,--gc-sections")
set(C_FLAGS_ALL_MODE "-std=c11 -fshort-enums")
set(LINK_ALL_MODE_FLAGS "${LINK_MAP_CREATION_FLAG} -nostartfiles -static-libgcc -T ${MEM_LD} -T ${SECTIONS_LD}")

if (CMAKE_BUILD_TYPE STREQUAL "Debug-target")
    add_definitions(-DDEBUG)
    set(CMAKE_C_FLAGS_INIT "${COMMON_FLAGS} -O0 -g3 ${C_FLAGS_ALL_MODE}")
    set(CMAKE_CXX_COMPILER_INIT "${COMMON_FLAGS} -O0 -g3 ${C_FLAGS_ALL_MODE}")
    set(CMAKE_EXE_LINKER_FLAGS_INIT "${COMMON_FLAGS} -O0 -g3 ${LINK_ALL_MODE_FLAGS}")
elseif (CMAKE_BUILD_TYPE STREQUAL "Release-target")
    set(CMAKE_C_FLAGS_INIT "${COMMON_FLAGS} -Os -g0 ${C_FLAGS_ALL_MODE}")
    set(CMAKE_CXX_COMPILER_INIT "${COMMON_FLAGS} -Os -g0 ${C_FLAGS_ALL_MODE}")
    set(CMAKE_EXE_LINKER_FLAGS_INIT "${COMMON_FLAGS} -Os -g0 ${LINK_ALL_MODE_FLAGS}")
endif ()

