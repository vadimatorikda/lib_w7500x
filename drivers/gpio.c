#include "mc_macroses.h"
#include "periph.h"

#include <stdbool.h>

void set_gpio_open_drain (pad_con_struct *GPIOx, uint8_t pin) {
    SET_BITS(GPIOx->REGISTER[pin], (1 << 3));
}

void reset_gpio_open_drain (pad_con_struct *GPIOx, uint8_t pin) {
    RESET_BITS(GPIOx->REGISTER[pin], (1 << 3));
}

void set_gpio_pull_off (pad_con_struct *GPIOx, uint16_t pin) {
    RESET_BITS(GPIOx->REGISTER[pin], (0b11 << 0));
}

void set_gpio_pull_up (pad_con_struct *GPIOx, uint16_t pin) {
    RESET_BITS(GPIOx->REGISTER[pin], (0b11 << 0));
    SET_BITS(GPIOx->REGISTER[pin], (0b10 << 0));
}

void set_gpio_pull_down (pad_con_struct *GPIOx, uint16_t pin) {
    RESET_BITS(GPIOx->REGISTER[pin], (0b11 << 0));
    SET_BITS(GPIOx->REGISTER[pin], (0b01 << 0));
}

void set_low_driving_strength (pad_con_struct *GPIOx, uint16_t pin) {
    SET_BITS(GPIOx->REGISTER[pin], (1 << 2));
}

void reset_low_driving_strength (pad_con_struct *GPIOx, uint16_t pin) {
    RESET_BITS(GPIOx->REGISTER[pin], (1 << 2));
}

void set_input_buffer (pad_con_struct *GPIOx, uint16_t pin) {
    SET_BITS(GPIOx->REGISTER[pin], (1 << 5));
}

void reset_input_buffer (pad_con_struct *GPIOx, uint16_t pin) {
    RESET_BITS(GPIOx->REGISTER[pin], (1 << 5));
}

void set_cmos (pad_con_struct *GPIOx, uint16_t pin) {
    SET_BITS(GPIOx->REGISTER[pin], (1 << 6));
}

void reset_cmos (pad_con_struct *GPIOx, uint16_t pin) {
    RESET_BITS(GPIOx->REGISTER[pin], (1 << 6));
}

void set_gpio_alt_func (afc_struct *GPIO, uint16_t pin, uint32_t func) {
    SET_VAL(GPIO->REGISTER[pin], func);
}

void set_gpio_out (gpio_struct *GPIOx, uint8_t pin) {
    SET_VAL(GPIOx->OUTENSET, 1 << pin);
}

void set_gpio_in (gpio_struct *GPIOx, uint8_t pin) {
    SET_VAL(GPIOx->OUTENCLR, 1 << pin);
}

bool gpio_get (gpio_struct *GPIO, uint16_t pin) {
    return GET_BITS_MSK(GPIO->DATA, 1 << pin);
}

void gpio_set (gpio_struct *GPIO, uint16_t pin) {
    SET_BITS(GPIO->DATAOUT, 1 << pin);
}

void gpio_reset (gpio_struct *GPIO, uint16_t pin) {
    RESET_BITS(GPIO->DATAOUT, 1 << pin);
}
