#include "core.h"

void nvic_irq_enable (uint8_t num) {
    NVIC->ISER = 1 << num;
}