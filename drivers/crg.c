#include "periph.h"
#include "mc_macroses.h"

#include <stdbool.h>

bool init_crg (uint32_t out_freq) {
    switch(out_freq) {
    case 48000000:
        SET_VAL(CRG->PLL_FCR, (4 << 16) | (1 << 8)); // 12 MHz * 4 / 1 * 1/1.
        break;

    default:
        return false;
    }

    SET_VAL(CRG->PLL_IFSR, 1); // External oscillator clock is PLL input clock source.
    SET_VAL(CRG->OSC_PDR, 1); // Internal 8MHz RC oscillator power down.

    return true;
}
