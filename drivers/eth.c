#include "periph.h"
#include "mc.h"
#include "vsrtos.h"

#include <string.h>

// GPIO.
#define GPIO_MIIM     GPIOB
#define AFC_MIIM      AFC_B

#define PIN_MIIM_MDC  14
#define PIN_MIIM_MDIO 15

static void init_miim_pins () {
    set_gpio_pull_down(PADCON_PB, 5);
    set_gpio_alt_func(AFC_B, 5, 1);
    set_gpio_pull_down(PADCON_PB, 12);

    set_gpio_pull_down(PADCON_PB, 6);
    set_gpio_alt_func(AFC_B, 6, 1);

    set_gpio_out(GPIO_MIIM, PIN_MIIM_MDC);
    set_gpio_out(GPIO_MIIM, PIN_MIIM_MDIO);
    set_gpio_alt_func(AFC_MIIM, PIN_MIIM_MDC, 1);
    set_gpio_alt_func(AFC_MIIM, PIN_MIIM_MDIO, 1);

    set_gpio_alt_func(AFC_D, 6, 1);
    set_gpio_pull_up(PADCON_PD, 6);
    reset_low_driving_strength(PADCON_PD, 6);
    reset_input_buffer(PADCON_PD, 6);
    reset_cmos(PADCON_PD, 6);
    gpio_set(GPIOD, 6);
    set_gpio_out(GPIOD, 6);
}

// GPIO_TRANS.
static void mdio_idle () {
    set_gpio_out(GPIO_MIIM, PIN_MIIM_MDIO);

    gpio_set(GPIO_MIIM, PIN_MIIM_MDC);
    vsrtos_delay(1);
    gpio_reset(GPIO_MIIM, PIN_MIIM_MDC);
    vsrtos_delay(1);
}

static void mdio_out (uint32_t val, uint32_t n) {
    for (val <<= (32 - n); n; val <<= 1, n--) {
        if (val & 0x80000000) {
            gpio_set(GPIO_MIIM, PIN_MIIM_MDIO);
        } else {
            gpio_reset(GPIO_MIIM, PIN_MIIM_MDIO);
        }

        vsrtos_delay(1);
        gpio_set(GPIO_MIIM, PIN_MIIM_MDC);
        vsrtos_delay(1);
        gpio_reset(GPIO_MIIM, PIN_MIIM_MDC);
    }
}

static void mdio_turnaround () {
    set_gpio_in(GPIO_MIIM, PIN_MIIM_MDIO);
    vsrtos_delay(1);
    gpio_set(GPIO_MIIM, PIN_MIIM_MDC);
    vsrtos_delay(1);
    gpio_reset(GPIO_MIIM, PIN_MIIM_MDC);
    vsrtos_delay(1);
}

static uint32_t mdio_in () {
    uint32_t i, val = 0;

    for (i = 0; i < 16; i++) {
        val <<= 1;
        gpio_set(GPIO_MIIM, PIN_MIIM_MDC);
        vsrtos_delay(1);
        gpio_reset(GPIO_MIIM, PIN_MIIM_MDC);
        vsrtos_delay(1);
        val |= gpio_get(GPIO_MIIM, PIN_MIIM_MDIO);
    }

    return (val);
}

// PHY_TRANS.
static uint32_t mdio_get_data (uint8_t phy_addr, uint32_t reg_addr) {
    uint8_t val = 0;

    mdio_out(0xFFFFFFFF, 32); // 32 consecutive ones on MDO to establish sync
    mdio_out(0x06, 4);        // start code 01, read command (10)
    mdio_out(phy_addr, 5);     // write PHY address
    mdio_out(reg_addr, 5);
    mdio_turnaround();        // turnaround MDO is tristated
    val = mdio_in();          // read the data value
    mdio_idle();              // turnaround MDO is tristated

    return val;
}

// PHY_FUNC
#define PHYREG_STATUS_LINK          (0x01UL<< 2)
#define PHYREG_STATUS               0x1
static bool get_phy_id (uint8_t *phy_addr) {
    for (uint32_t i = 0; i < 8; i++) {
        mdio_out(0xFFFFFFFF, 32);     // 32 Consecutive ones on MDO to establish sync.
        mdio_out(0x06, 4);            // Start code 01, read command (10).
        mdio_out(i, 5);               // Write PHY address.
        mdio_out(PHYREG_STATUS, 5);
        mdio_turnaround();            // Turnaround MDO is tristated.
        uint32_t data = mdio_in();    // Read the data value.
        mdio_idle();                  // Turnaround MDO is tristated.
        if ((data != 0x0000) && (data != 0xFFFF)) {
            *phy_addr = i;
            return true;
        }
    }

    return false;
}

bool phy_init (uint8_t *phy_addr) {
    init_miim_pins();

    if (!get_phy_id(phy_addr)) {
        return false;
    }

    return true;
}

bool phy_is_link (uint8_t phy_addr) {
    return mdio_get_data(phy_addr, PHYREG_STATUS) & PHYREG_STATUS_LINK;
}

// SOCKET_UDP
void socket_udp_close (eth_socket_struct *socket) {
    socket->CR = 0x10;           // Send cmd close socket.
    while (socket->CR != 0x00) { // Wait getting command.
        vsrtos_delay(1);
    }
    socket->ICR = 0xFF;          // Clear interrupt flags.
    while (socket->SR != 0x00) { // Wait closing socket.
        vsrtos_delay(1);
    }
}

void socket_udp_open (eth_socket_struct *socket, uint16_t port) {
    socket->MR = 0b0010;         // Protocol: UDP.
    socket->PORT = port;
    socket->CR = 0x01;           // Send command to open UDP socket.
    while (socket->CR == 0x00) { // Wait getting command.
        vsrtos_delay(1);
    }
    while (socket->SR == 0x00) { // Wait opening socket.
        vsrtos_delay(1);
    }
}

bool socket_udp_send (eth_socket_struct *socket, eth_socket_data *s_data,
                      void *buf, uint16_t len, uint32_t dst_adr, uint16_t dst_port) {
    if (socket->MR != 0b0010) { // Port is not in UDP mode?
        return false;
    }

    if (socket->SR != 0x22) {  // Port is not open?
        return false;
    }

    if (len > (socket->TXBUF_SIZE << 10)) { // Check size.
        return false;
    }

    socket->DIPR = dst_adr;
    socket->DPORT = dst_port;

    uint16_t ptr = socket->TX_WR;
    for (uint16_t i = 0; i < len; i++)
        *(volatile uint8_t *)((uint8_t *)s_data + ((ptr + i) & 0xFFFF)) = ((uint8_t *)buf)[i];
    socket->TX_WR += len;

    socket->CR = 0x20;     // Send command to send UDP message.
    while (socket->CR) {   // Wait getting command.
        vsrtos_delay(1);
    }

    while (true) {
        if (socket->ISR & (1 << 4)) { // Complete sending
            socket->ICR = (1 << 4);
            return true;
        } else if (socket->ISR & (1 << 3)) { // Check timeout.
            socket->ICR = (1 << 3);
            return false;
        } else if (socket->ISR & (1 << 1)) { // Disconnect.
            socket->ICR = (1 << 1);
            return false;
        }
        vsrtos_delay(1);
    }
}

