#ifndef MC_MACROSES_H
#define MC_MACROSES_H

#define SET_BITS(REG, BITS) (REG) |= (BITS)
#define RESET_BITS(REG, BITS) (REG) &= ~(BITS)

#define SET_VAL(REG, VAL) (REG) = ((uint32_t)VAL)

#define GET_BITS_MSK(REG, MSK) ((REG) & (MSK))

#define BITS_IS_SET(REG, VAL) (GET_BITS_MSK(REG, VAL) == (VAL))
#define BIT_IS_SET(REG, VAL) (GET_BITS_MSK(REG, VAL))

#define WHILE_TIME(EXPRESSION, WAIT_CYCLES)      \
    do {                                        \
        uint32_t wait = WAIT_CYCLES;            \
                                                \
        while((EXPRESSION) && wait) {           \
            wait--;                             \
        };                                      \
                                                \
        if (wait == 0) {                        \
            return false;                       \
        }                                       \
    } while (false)


#define WORD_SIZE(BYTE_SIZE) (uint32_t)((BYTE_SIZE / 4) + ((BYTE_SIZE % 4)?1:0))

#endif // MC_MACROSES_H
