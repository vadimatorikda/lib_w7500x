#ifndef MC_H
#define MC_H

#include "vsrtos.h"
#include "periph.h"
#include "core.h"

#include <stdbool.h>
#include <stdint.h>

/********************************************************************************
 * SYSTEM
 ********************************************************************************/
void init_system ();

/********************************************************************************
 * MODE
 ********************************************************************************/
void go_to_stadby_mode ();

/********************************************************************************
 * NVIC
 ********************************************************************************/
void nvic_irq_enable (uint8_t num);

/********************************************************************************
 * Core
 ********************************************************************************/
__attribute__((always_inline)) static inline void enable_all_irq () {
    __asm("cpsie i");
}

__attribute__((always_inline)) static inline void disable_all_irq () {
    __asm("cpsid i");
}

__attribute__((always_inline)) static inline void wfi () {
    __asm("WFI");
}

__attribute__((always_inline)) static inline void mc_reset () {
    SCB->AIRCR = (0x05FA << 16) | (1 << 2);
}

/********************************************************************************
 * GPIO
 ********************************************************************************/
void set_gpio_open_drain (pad_con_struct *GPIOx, uint8_t pin);
void reset_gpio_open_drain (pad_con_struct *GPIOx, uint8_t pin);

void set_gpio_pull_off (pad_con_struct *GPIOx, uint16_t pin);
void set_gpio_pull_up (pad_con_struct *GPIOx, uint16_t pin);
void set_gpio_pull_down (pad_con_struct *GPIOx, uint16_t pin);

void set_low_driving_strength (pad_con_struct *GPIOx, uint16_t pin);
void reset_low_driving_strength (pad_con_struct *GPIOx, uint16_t pin);

void set_input_buffer (pad_con_struct *GPIOx, uint16_t pin);
void reset_input_buffer (pad_con_struct *GPIOx, uint16_t pin);

void set_cmos (pad_con_struct *GPIOx, uint16_t pin);
void reset_cmos (pad_con_struct *GPIOx, uint16_t pin);

void set_gpio_alt_func (afc_struct *GPIO, uint16_t pin, uint32_t func);

void set_gpio_out (gpio_struct *GPIOx, uint8_t pin);
void set_gpio_in (gpio_struct *GPIOx, uint8_t pin);

bool gpio_get (gpio_struct *GPIO, uint16_t pin);
void gpio_set (gpio_struct *GPIO, uint16_t pin);
void gpio_reset (gpio_struct *GPIO, uint16_t pin);

/********************************************************************************
 * DMA
 ********************************************************************************/
void init_dma ();

/********************************************************************************
 * CRG
 ********************************************************************************/
bool init_crg (uint32_t out_freq);

/********************************************************************************
* ETH
********************************************************************************/
#define SET_ETH_CLK() ETH->TIC100US = (CPU_FREQ/10000)

#define SET_ETH_MAC(X5, X4, X3, X2, X1, X0) \
    ETH->SHAR0 = ((X1) << 24) | ((X0) << 16); \
    ETH->SHAR1 = ((X5) << 24) | ((X4) << 16) | ((X3) << 8) | ((X2) << 0);

#define SET_ETH_IP(X3, X2, X1, X0) \
    (ETH->SIPR = ((X3) << 24) | ((X2) << 16) | ((X1) << 8) | ((X0) << 0))

#define SET_ETH_GW(X3, X2, X1, X0) \
    (ETH->GAR = ((X3) << 24) | ((X2) << 16) | ((X1) << 8) | ((X0) << 0))

#define SET_ETH_MSK(X3, X2, X1, X0) \
    (ETH->SUBR = ((X3) << 24) | ((X2) << 16) | ((X1) << 8) | ((X0) << 0))

#define SET_SOCKET_TX_IP(X3, X2, X1, X0) \
    (((X3) << 24) | ((X2) << 16) | ((X1) << 8) | ((X0) << 0))

bool phy_init (uint8_t *phy_addr);
bool phy_is_link (uint8_t phy_addr);

void socket_udp_close (eth_socket_struct *socket);
void socket_udp_open (eth_socket_struct *socket, uint16_t port);
bool socket_udp_send (eth_socket_struct *socket, eth_socket_data *s_data,
                      void *buf, uint16_t len, uint32_t dst_adr, uint16_t dst_port);

#endif // MC_H
