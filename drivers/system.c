#include <stdint.h>

#define W7500x_TRIM_BGT          ((volatile uint32_t *)(0x41001210))
#define W7500x_TRIM_OSC          ((volatile uint32_t *)(0x41001004))

#define W7500x_INFO_BGT          ((volatile uint32_t *)(0x0003FDB8))
#define W7500x_INFO_OSC          ((volatile uint32_t *)(0x0003FDBC))

void init_system () {
    *W7500x_TRIM_BGT = *W7500x_INFO_BGT;
    *W7500x_TRIM_OSC = *W7500x_INFO_OSC;
}