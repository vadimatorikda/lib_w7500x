#include <stdint.h>
#include <stdbool.h>

void dummy () {
    while (true);
}

void HardFault_Handler () {
    dummy();
};

void Reset_Handler () __attribute__((weak, alias("dummy")));
void NMI_Handler () __attribute__((weak, alias("dummy")));
void SVC_Handler () __attribute__((weak, alias("dummy")));
void PendSV_Handler () __attribute__((weak, alias("dummy")));
void SysTick_Handler () __attribute__((weak, alias("dummy")));

void SSP0_Handler () __attribute__((weak, alias("dummy")));
void SSP1_Handler () __attribute__((weak, alias("dummy")));
void UART0_Handler () __attribute__((weak, alias("dummy")));
void UART1_Handler () __attribute__((weak, alias("dummy")));
void UART2_Handler () __attribute__((weak, alias("dummy")));
void I2C0_Handler () __attribute__((weak, alias("dummy")));
void I2C1_Handler () __attribute__((weak, alias("dummy")));
void PORT0_Handler () __attribute__((weak, alias("dummy")));
void PORT1_Handler () __attribute__((weak, alias("dummy")));
void PORT2_Handler () __attribute__((weak, alias("dummy")));
void PORT3_Handler () __attribute__((weak, alias("dummy")));
void DMA_Handler () __attribute__((weak, alias("dummy")));
void DUALTIMER0_Handler () __attribute__((weak, alias("dummy")));
void DUALTIMER1_Handler () __attribute__((weak, alias("dummy")));
void PWM0_Handler () __attribute__((weak, alias("dummy")));
void PWM1_Handler () __attribute__((weak, alias("dummy")));
void PWM2_Handler () __attribute__((weak, alias("dummy")));
void PWM3_Handler () __attribute__((weak, alias("dummy")));
void PWM4_Handler () __attribute__((weak, alias("dummy")));
void PWM5_Handler () __attribute__((weak, alias("dummy")));
void PWM6_Handler () __attribute__((weak, alias("dummy")));
void PWM7_Handler () __attribute__((weak, alias("dummy")));
void RTC_Handler () __attribute__((weak, alias("dummy")));
void ADC_Handler () __attribute__((weak, alias("dummy")));
void WZTOE_Handler () __attribute__((weak, alias("dummy")));
void EXTI_Handler () __attribute__((weak, alias("dummy")));

typedef void (*irq_handler) ();

typedef struct __attribute__((packed)) {
    uint32_t stack;
    irq_handler irq[15 + 32];
} irq_vector;

extern char __section_stack_msp_end;

__attribute__ ((section (".isr_vector")))
const irq_vector vector = {
    .stack = (uint32_t)&__section_stack_msp_end,   // 0x0000_0000
    .irq = {
        Reset_Handler,                             // 0x0000_0004
        NMI_Handler,                               // 0x0000_0008
        HardFault_Handler,                         // 0x0000_000C
        0,                                         // 0x0000_0010
        0,                                         // 0x0000_0014
        0,                                         // 0x0000_0018
        0,                                         // 0x0000_001C
        0,                                         // 0x0000_0020
        0,                                         // 0x0000_0024
        0,                                         // 0x0000_0028
        SVC_Handler,                               // 0x0000_002C
        0,                                         // 0x0000_0030
        0,                                         // 0x0000_0034
        PendSV_Handler,                            // 0x0000_0038
        SysTick_Handler,                           // 0x0000_003C

        SSP0_Handler,                              // 0x0000_0040
        SSP1_Handler,                              // 0x0000_0044
        UART0_Handler,                             // 0x0000_0048
        UART1_Handler,                             // 0x0000_004C
        UART2_Handler,                             // 0x0000_0050
        I2C0_Handler,                              // 0x0000_0054
        I2C1_Handler,                              // 0x0000_0058
        PORT0_Handler,                             // 0x0000_005C
        PORT1_Handler,                             // 0x0000_0060
        PORT2_Handler,                             // 0x0000_0064
        PORT3_Handler,                             // 0x0000_0068
        DMA_Handler,                               // 0x0000_006C
        DUALTIMER0_Handler,                        // 0x0000_0070
        DUALTIMER1_Handler,                        // 0x0000_0074
        PWM0_Handler,                              // 0x0000_0078
        PWM1_Handler,                              // 0x0000_007C
        PWM2_Handler,                              // 0x0000_0080
        PWM3_Handler,                              // 0x0000_0084
        PWM4_Handler,                              // 0x0000_0088
        PWM5_Handler,                              // 0x0000_008C
        PWM6_Handler,                              // 0x0000_0090
        PWM7_Handler,                              // 0x0000_0094
        RTC_Handler,                               // 0x0000_00A0
        ADC_Handler,                               // 0x0000_00A4
        WZTOE_Handler,                             // 0x0000_00A8
        EXTI_Handler,                              // 0x0000_00AC
    }
};
