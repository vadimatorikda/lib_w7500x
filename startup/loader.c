#include "mc.h"

#include <stdint.h>

void __attribute__((noreturn)) main ();

extern uint32_t __section_bss_start;
extern uint32_t __section_bss_end;

extern uint32_t __section_flash_data_start;
extern uint32_t __section_ram_data_start;
extern uint32_t __section_ram_data_end;

extern uint32_t __section_stack_msp_start;
extern uint32_t __section_stack_msp_end;

__attribute__((always_inline))
inline static void cpy_data (uint32_t *src, uint32_t *start, uint32_t *end) {
    uint32_t *p = start;
    while (p < end) {
        *p++ = *src++;
    }
}

__attribute__((always_inline))
inline static void set_val (uint32_t *start, uint32_t *end, uint32_t value) {
    uint32_t *p = start;
    while (p < end) {
        *p++ = value;
    }
}

__attribute__((always_inline))
inline static void init_section_data () {
    cpy_data(&__section_flash_data_start, &__section_ram_data_start, &__section_ram_data_end);
}

inline static void __attribute__((always_inline)) init_section_bss () {
    set_val(&__section_bss_start, &__section_bss_end, 0);
}

inline static void __attribute__((always_inline)) init_section_stack () {
    set_val(&__section_stack_msp_start, &__section_stack_msp_end, 0xA3A33A3A);
}

void __attribute__((noreturn)) Reset_Handler () {
    init_section_stack();
    init_section_bss();
    init_section_data();
    main();
    mc_reset();
}
