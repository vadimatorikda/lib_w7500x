#ifndef CORE_H
#define CORE_H

#include <stdint.h>

typedef struct {
    volatile uint32_t ISER;
    volatile uint32_t RES_1[31];
    volatile uint32_t ICER;
    volatile uint32_t RES_2[31];
    volatile uint32_t ISPR;
    volatile uint32_t RES_3[31];
    volatile uint32_t ICPR;
    volatile uint32_t RES_4[31];
    volatile uint32_t IPR[8];
} nvic_struct;

typedef struct {
    volatile uint32_t CSR;
    volatile uint32_t RVR;
    volatile uint32_t CVR;
    volatile uint32_t CALIB;
} systic_struct;

typedef struct {
    volatile uint32_t CPUID;
    volatile uint32_t ICSR;
    volatile uint32_t RES_1;
    volatile uint32_t AIRCR;
    volatile uint32_t SCR;
    volatile uint32_t CCR;
    volatile uint32_t RES_2;
    volatile uint32_t SHPR2;
    volatile uint32_t SHPR3;
} scb_struct;

#define SYSTIC    ((systic_struct *)       0xE000E010)
#define NVIC      ((nvic_struct *)         0xE000E100)
#define SCB       ((scb_struct *)          0xE000ED00)

#endif // CORE_H
