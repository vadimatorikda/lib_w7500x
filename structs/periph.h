#ifndef PERIPH_H
#define PERIPH_H

#include <stdint.h>

#define PINS_NUMBER 16

typedef struct {
    uint32_t OSC_PDR;
    uint32_t RES_0[3];
    uint32_t PLL_PDR;
    uint32_t PLL_FCR;
    uint32_t PLL_OER;
    uint32_t PLL_BPR;
    uint32_t PLL_IFSR;
    uint32_t RES_1[3];
    uint32_t FCLK_SSR;
    uint32_t FCLK_PVSR;
    uint32_t RES_2[2];
    uint32_t SSPCLK_SSR;
    uint32_t SSPCLK_PVSR;
    uint32_t RES_3[6];
    uint32_t ADCCLK_SSR;
    uint32_t ADCCLK_PVSR;
    uint32_t RES_4[2];
    uint32_t TIMER0CLK_SSR;
    uint32_t TIMER0CLK_PVSR;
    uint32_t RES_5[2];
    uint32_t TIMER1CLK_SSR;
    uint32_t TIMER1CLK_PVSR;
    uint32_t RES_6[10];
    uint32_t PWM0CLK_SSR;
    uint32_t PWM0CLK_PVSR;
    uint32_t RES_7[2];
    uint32_t PWM1CLK_SSR;
    uint32_t PWM1CLK_PVSR;
    uint32_t RES_8[2];
    uint32_t PWM2CLK_SSR;
    uint32_t PWM2CLK_PVSR;
    uint32_t RES_9[2];
    uint32_t PWM3CLK_SSR;
    uint32_t PWM3CLK_PVSR;
    uint32_t RES_10[2];
    uint32_t PWM4CLK_SSR;
    uint32_t PWM4CLK_PVSR;
    uint32_t RES_11[2];
    uint32_t PWM5CLK_SSR;
    uint32_t PWM5CLK_PVSR;
    uint32_t RES_12[2];
    uint32_t PWM6CLK_SSR;
    uint32_t PWM6CLK_PVSR;
    uint32_t RES_13[2];
    uint32_t PWM7CLK_SSR;
    uint32_t PWM7CLK_PVSR;
    uint32_t RES_14[4];
    uint32_t RES_15[2];
    uint32_t WDOGCLK_HS_SSR;
    uint32_t WDOGCLK_HS_PVSR;
    uint32_t RES_16[2];
    uint32_t UARTCLK_SSR;
    uint32_t UARTCLK_PVSR;
    uint32_t RES_17[2];
    uint32_t MIICLK_ECR;
    uint32_t RES_18[3];
    uint32_t MONCLK_SSR;
} crg_struct;

typedef struct {
    uint32_t DR;
    union {
        uint32_t RSR;
        uint32_t ECR;
    } STATUS;
    uint32_t RES_0[4];
    uint32_t FR;
    uint32_t RES_1;
    uint32_t ILPR;
    uint32_t IBRD;
    uint32_t FBRD;
    uint32_t LCR_H;
    uint32_t CR;
    uint32_t IFLS;
    uint32_t IMSC;
    uint32_t RIS;
    uint32_t MIS;
    uint32_t ICR;
    uint32_t DMACR;
} uart_struct;

typedef struct {
    uint32_t DATA;
    uint32_t STATE;
    uint32_t CTRL;
    union {
        uint32_t STATUS;
        uint32_t CLEAR;
    } INT;
    uint32_t BAUDDIV;
} simple_uart_struct;

typedef struct {
    uint32_t ADC_CTR;
    uint32_t ADC_CHSEL;
    uint32_t ADC_START;
    uint32_t ADC_DATA;
    uint32_t ADC_INT;
    uint32_t RES_0[2];
    uint32_t ADC_INTCLR;
} adc_struct;

typedef struct {
    uint32_t TimerLoad;
    uint32_t TimerValue;
    uint32_t TimerControl;
    uint32_t TimerIntClr;
    uint32_t TimerRIS;
    uint32_t TimerMIS;
    uint32_t TimerBGLoad;
} dual_timer_struct;

typedef struct {
    uint32_t DATA;
    uint32_t DATAOUT;
    uint32_t RES_0[2];
    uint32_t OUTENSET;
    uint32_t OUTENCLR;
    uint32_t RES_1;
    uint32_t RES_2;
    uint32_t INTENSET;
    uint32_t INTENCLR;
    uint32_t INTTYPESET;
    uint32_t INTTYPECLR;
    uint32_t INTPOLSET;
    uint32_t INTPOLCLR;
    union {
        uint32_t INTSTATUS;
        uint32_t INTCLEAR;
    } Interrupt;
    uint32_t RES_3[241];
    uint32_t LB_MASKED[256];
    uint32_t UB_MASKED[256];
} gpio_struct;

typedef struct {
    uint32_t IER;
    uint32_t SSR;
    uint32_t PSR;
} pwm_struct;

typedef struct {
    uint32_t IR;
    uint32_t IER;
    uint32_t ICR;
    uint32_t TCR;
    uint32_t PCR;
    uint32_t PR;
    uint32_t MR;
    uint32_t LR;
    uint32_t UDMR;
    uint32_t TCMR;
    uint32_t PEEER;
    uint32_t CMR;
    uint32_t CR;
    uint32_t PDMR;
    uint32_t DZER;
    uint32_t DZCR;
} pwm_ch_struct;

typedef struct {
    uint32_t RNG_RUN;
    uint32_t RNG_SEED;
    uint32_t RNG_CLKSEL;
    uint32_t RNG_MODE;
    uint32_t RNG_RN;
    uint32_t RNG_POLY;
} rng_struct;

typedef struct {
    uint32_t CR0;
    uint32_t CR1;
    uint32_t DR;
    uint32_t SR;
    uint32_t CPSR;
    uint32_t IMSC;
    uint32_t RIS;
    uint32_t MIS;
    uint32_t ICR;
    uint32_t DMACR;
} ssp_struct;

typedef struct {
    uint32_t LOAD;
    uint32_t VALUE;
    uint32_t CONTROL;
    uint32_t INT_CLR;
    uint32_t RIS;
    uint32_t MIS;
    uint32_t RES_0[762];
    uint32_t LOCK;
} wdt_struct;

typedef struct {
    uint32_t REGISTER[16];
} afc_struct;

typedef struct {
    uint32_t REGISTER[16];
} pad_con_struct;

typedef struct {
    uint32_t REGISTER[16];
} exti_struct;

typedef struct {
    uint32_t VERSIONR;            // 0x0000
    uint32_t RES_0[2047];
    uint32_t TIC100US;            // 0x2000
    uint32_t RES_1[63];
    uint32_t IR;                  // 0x2100
    uint32_t IMR;                 // 0x2104
    uint32_t ICR;                 // 0x2108
    uint32_t RES_2;
    uint32_t SIR;                 // 0x2110
    uint32_t SIMR;                // 0x2114
    uint32_t RES_3[58];
    uint32_t INTLEVEL;            // 0x2200
    uint32_t RES_4[63];
    uint32_t MR;                  // 0x2300
    uint32_t RES_5[63];
    uint32_t PTIMER;              // 0x2400
    uint32_t PMAGIC;              // 0x2404
    uint32_t PHAR1;               // 0x2408
    uint32_t PHAR0;               // 0x240C
    uint32_t PSID;                // 0x2410
    uint32_t PMRU;                // 0x2414
    uint32_t RES_6[3834];
    uint32_t SHAR1;               // 0x6000
    uint32_t SHAR0;               // 0x6004
    uint32_t GAR;                 // 0x6008
    uint32_t SUBR;                // 0x600C
    uint32_t SIPR;                // 0x6010
    uint32_t RES_7[3];
    uint32_t NETCFGLOCK;          // 0x6020
    uint32_t RES_8[7];
    uint32_t RTR;                 // 0x6040
    uint32_t RCR;                 // 0x6044
    uint32_t RES_9[2];
    uint32_t UIPR;                // 0x6050
    uint32_t UPORTR;              // 0x6054
} eth_struct;

typedef struct {
    uint32_t MR;                  // 0x0000
    uint32_t RES_0[3];
    uint32_t CR;                  // 0x0010
    uint32_t RES_1[3];
    uint32_t ISR;                 // 0x0020
    uint32_t IMR;                 // 0x0024
    uint32_t ICR;                 // 0x0028
    uint32_t RES_2;
    uint32_t SR;                  // 0x0030
    uint32_t RES_3[51];
    uint32_t PROTO;               // 0x0100
    uint32_t TOS;                 // 0x0104
    uint32_t TTL;                 // 0x0108
    uint32_t FRG;                 // 0x010C
    uint32_t MSSR;                // 0x0110
    uint32_t PORT;                // 0x0114
    uint32_t DHAR0;               // 0x0118
    uint32_t DHAR1;               // 0x011C
    uint32_t DPORT;               // 0x0120
    uint32_t DIPR;                // 0x0124
    uint32_t RES_4[22];
    uint32_t KPALVTR;             // 0x0180
    uint32_t RTR;                 // 0x0184
    uint32_t RCR;                 // 0x0188
    uint32_t RES_5[29];
    uint32_t TXBUF_SIZE;          // 0x0200
    uint32_t TX_FSR;              // 0x0204
    uint32_t TX_RD;               // 0x0208
    uint32_t TX_WR;               // 0x020C
    uint32_t RES_6[4];
    uint32_t RXBUF_SIZE;          // 0x0220
    uint32_t RX_RSR;              // 0x0224
    uint32_t RX_RD;               // 0x0228
    uint32_t RX_WR;               // 0x022C
} eth_socket_struct;

typedef struct {
    uint8_t data[2048];
} eth_socket_data;

#define CRG             ((crg_struct *)            0x41001000)
#define AFC_A           ((afc_struct *)            0x41002000)
#define AFC_B           ((afc_struct *)            0x41002040)
#define AFC_C           ((afc_struct *)            0x41002080)
#define AFC_D           ((afc_struct *)            0x410020C0)
#define EXTI_PA         ((exti_struct *)           0x41002200)
#define EXTI_PB         ((exti_struct *)           0x41002240)
#define EXTI_PC         ((exti_struct *)           0x41002280)
#define EXTI_PD         ((exti_struct *)           0x410022C0)
#define PADCON_PA       ((pad_con_struct *)        0x41003000)
#define PADCON_PB       ((pad_con_struct *)        0x41003040)
#define PADCON_PC       ((pad_con_struct *)        0x41003080)
#define PADCON_PD       ((pad_con_struct *)        0x410030C0)
#define GPIOA           ((gpio_struct *)           0x42000000)
#define GPIOB           ((gpio_struct *)           0x43000000)
#define GPIOC           ((gpio_struct *)           0x44000000)
#define GPIOD           ((gpio_struct *)           0x45000000)

#define ETH             ((eth_struct *)            0x46000000)
#define ETH_SOCKET_0    ((eth_socket_struct *)     0x46010000)
#define ETH_SOCKET_0_TX ((eth_socket_data *)       0x46020000)
#define ETH_SOCKET_0_RX ((eth_socket_data *)       0x46030000)
#define ETH_SOCKET_1    ((eth_socket_struct *)     0x46050000)
#define ETH_SOCKET_1_TX ((eth_socket_data *)       0x46060000)
#define ETH_SOCKET_1_RX ((eth_socket_data *)       0x46070000)
#define ETH_SOCKET_2    ((eth_socket_struct *)     0x46090000)
#define ETH_SOCKET_2_TX ((eth_socket_data *)       0x460A0000)
#define ETH_SOCKET_2_RX ((eth_socket_data *)       0x460B0000)
#define ETH_SOCKET_3    ((eth_socket_struct *)     0x460D0000)
#define ETH_SOCKET_3_TX ((eth_socket_data *)       0x460E0000)
#define ETH_SOCKET_3_RX ((eth_socket_data *)       0x460F0000)
#define ETH_SOCKET_4    ((eth_socket_struct *)     0x46110000)
#define ETH_SOCKET_4_TX ((eth_socket_data *)       0x46120000)
#define ETH_SOCKET_4_RX ((eth_socket_data *)       0x46130000)
#define ETH_SOCKET_5    ((eth_socket_struct *)     0x46150000)
#define ETH_SOCKET_5_TX ((eth_socket_data *)       0x46160000)
#define ETH_SOCKET_5_RX ((eth_socket_data *)       0x46170000)
#define ETH_SOCKET_6    ((eth_socket_struct *)     0x46190000)
#define ETH_SOCKET_6_TX ((eth_socket_data *)       0x461A0000)
#define ETH_SOCKET_6_RX ((eth_socket_data *)       0x461B0000)
#define ETH_SOCKET_7    ((eth_socket_struct *)     0x461D0000)
#define ETH_SOCKET_7_TX ((eth_socket_data *)       0x461E0000)
#define ETH_SOCKET_7_RX ((eth_socket_data *)       0x461F0000)

#endif // PERIPH_H
